using System;
using System.Threading;
using System.Text;
using RabbitMQ.Client.Events;
using RabbitMQ;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper._channel.QueueDeclare(queue: "Ping_Queue",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

            Wrapper._consumer.Received += MessageReceived;

            Wrapper.SendMessageToQueue(
                massage: $"Час: {DateTime.Now} | Ping! ",
                key: "Pong_Queue");

            Wrapper.ListenQueue(queueName: "Ping_Queue");

            Console.ReadLine();
            Console.WriteLine("Pinger Completed");     
        }


        public static void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var messege = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Час отримання: {DateTime.Now}, Message: {messege}");

            Thread.Sleep(2500);

            Wrapper.SendMessageToQueue(
                massage: $"{DateTime.Now} | Ping! ",
                key: "Pong_Queue");

            Wrapper._channel.BasicAck(args.DeliveryTag, false);          
        }
    }
}
