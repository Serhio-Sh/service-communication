using System;
using System.Threading;
using System.Text;
using RabbitMQ.Client.Events;
using RabbitMQ;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper._channel.QueueDeclare(queue: "Pong_Queue",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

            Wrapper._consumer.Received += MessageReceived;

            Wrapper.ListenQueue(queueName: "Pong_Queue");

            Console.ReadLine();
            Console.WriteLine("Ponger Completed");         
        }


        public static void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var messege = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Час отримання: {DateTime.Now}, Message: {messege}");

            Thread.Sleep(2500);

            Wrapper.SendMessageToQueue(
                massage: $"{DateTime.Now} | Pong! ",
                key: "Ping_Queue");

            Wrapper._channel.BasicAck(args.DeliveryTag, false);     
        }
    }
}
