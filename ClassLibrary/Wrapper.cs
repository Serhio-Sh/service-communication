using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ
{
    public static class Wrapper
    {
        private static ConnectionFactory _factory { get; set; }
        public static IModel _channel { get; set; }
        private static IConnection _connection { get; set; }
        public static EventingBasicConsumer _consumer { get; set; }

        static Wrapper()
        {
            _factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _consumer = new EventingBasicConsumer(_channel);
        }


        public static void ListenQueue(string queueName)
        {
            _channel.BasicConsume(queue: queueName,
                    autoAck: false,
                    consumer: _consumer);        
        }

        public static void SendMessageToQueue(string massage, string key)
        {
            var body = Encoding.UTF8.GetBytes(massage);

            _channel.BasicPublish(
                exchange: "",
                routingKey: key,
                basicProperties: null,
                body: body);          
        }

        public static void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}